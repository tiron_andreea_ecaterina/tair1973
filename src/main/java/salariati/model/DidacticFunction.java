package salariati.model;

public enum DidacticFunction {
    ASISTENT, CONFERENT, LECTURER, TEACHER
}
