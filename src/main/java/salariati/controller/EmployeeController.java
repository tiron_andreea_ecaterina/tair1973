package salariati.controller;

import java.util.Comparator;
import java.util.List;

import salariati.model.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.EmployeeRepositoryInterface;

public class EmployeeController {

    private EmployeeRepositoryInterface employeeRepository;

    public EmployeeController(EmployeeRepositoryInterface employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public boolean addEmployee(Employee employee) {
        boolean saved = employeeRepository.addEmployee(employee);
        if (!saved)
            System.out.println("Nu se poate salva salariatul");
        return saved;
    }

    public List<Employee> printAllSortedEmployees() {
        List<Employee> employeeList = getEmployeesList();
        if (employeeList.isEmpty()) {
            System.out.println("Nu exista salariati");
        } else {
            employeeList.sort(Comparator.comparing((Employee e) -> e.getSalary()).thenComparing(e -> e.getCnp().substring(1)).reversed());
            System.out.println("\nLISTA SALARIATI: ");
            for (Employee employee : employeeList)
                System.out.println(employee);

        }
        return employeeList;
    }

    public void modifyEmployee(String cnp, DidacticFunction function) {
        Employee oldEmployee = employeeRepository.getEmployeeByCriteria(cnp);
        if (oldEmployee != null) {
            Employee newEmployee = new Employee(oldEmployee.getFirstName(), oldEmployee.getLastName(), oldEmployee.getCnp(), function, oldEmployee.getSalary());
            employeeRepository.modifyEmployee(oldEmployee, newEmployee);
        } else
            System.out.println("Nu exista salariat cu acest cnp");
    }

    public List<Employee> getEmployeesList() {
        return employeeRepository.getEmployeeList();
    }
}
