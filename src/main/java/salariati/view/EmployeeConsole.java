package salariati.view;

import salariati.controller.EmployeeController;
import salariati.model.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.EmployeeRepositoryFromFile;
import salariati.repository.EmployeeRepositoryInterface;

import java.util.Scanner;

public class EmployeeConsole {
    private EmployeeController employeeController;
    private Scanner scanner;

    public EmployeeConsole() {
        EmployeeRepositoryInterface employeeRepositoryInterface = new EmployeeRepositoryFromFile();
        this.employeeController = new EmployeeController(employeeRepositoryInterface);
        scanner = new Scanner(System.in);
    }

    private void printMenu() {
        System.out.println();
        System.out.println("SALARIATI:");
        System.out.println("\t1. Adauga un nou salariat");
        System.out.println("\t2. Modifica functia unui salariat");
        System.out.println("\t3. Afiseaza salariatii ordonati");
        System.out.println("\t0. Exit");
        System.out.print("Alege optiune: ");
    }

    public void start() {
        boolean exit = false;
        while (!exit) {
            printMenu();
            String option = scanner.next();
            switch (option) {
                case "1":
                    System.out.println("Dati prenume salariat: ");
                    String firstName = scanner.next();
                    System.out.println("Dati nume salariat: ");
                    String lastName = scanner.next();
                    System.out.println("Dati cnp salariat: ");
                    String cnp = scanner.next();
                    System.out.println("Dati functie salariat: ");
                    String functie = scanner.next();
                    functie = functie.toUpperCase();
                    DidacticFunction functieDidactica = getDidacticFunction(functie);
                    System.out.println("Dati salariu incadrare salariat: ");
                    String salariu = scanner.next();

                    if (functieDidactica != null) {
                        Employee employee = new Employee(firstName, lastName, cnp, functieDidactica, salariu);
                        addEmployee(employee);
                    } else
                        System.out.println("Functie salariat incorecta !");
                    break;
                case "2":
                    System.out.println("Dati cnp salariat: ");
                    String cnpSalariat = scanner.next();
                    System.out.println("Dati o noua functie salariat: ");
                    String functieNoua = scanner.next();
                    functieNoua = functieNoua.toUpperCase();
                    modifyEmployee(cnpSalariat, functieNoua);
                    break;
                case "3":
                    printSortedAllEmployees();
                    break;
                case "0":
                    exit = true;
                    break;
                default:
                    System.out.println("Dati o optiune corecta !");
            }
        }
    }

    private void printSortedAllEmployees() {
        employeeController.printAllSortedEmployees();
    }

    private void modifyEmployee(String cnp, String function) {
        DidacticFunction didacticFunction = getDidacticFunction(function);
        employeeController.modifyEmployee(cnp, didacticFunction);
    }

    private void addEmployee(Employee employee) {
        employeeController.addEmployee(employee);
    }

    private DidacticFunction getDidacticFunction(String function) {
        if (function.equals("ASISTENT"))
            return DidacticFunction.ASISTENT;
        if (function.equals("LECTURER"))
            return DidacticFunction.LECTURER;
        if (function.equals("TEACHER"))
            return DidacticFunction.TEACHER;
        if (function.equals("CONFERENT"))
            return DidacticFunction.CONFERENT;
        return null;
    }
}