package salariati.repository;

import java.util.ArrayList;
import java.util.List;

import salariati.model.DidacticFunction;

import salariati.model.Employee;
import salariati.validator.EmployeeValidator;

public class EmployeeRepositoryMock implements EmployeeRepositoryInterface {

    private List<Employee> employeeList;
    private EmployeeValidator employeeValidator;

    public EmployeeRepositoryMock() {

        employeeValidator = new EmployeeValidator();
        employeeList = new ArrayList<Employee>();

        Employee e1 = new Employee("Ana", "Pacuraru", "1234567890876", DidacticFunction.ASISTENT, "2500");
        Employee e2 = new Employee("Marian", "Dumitrescu", "1234567890876", DidacticFunction.LECTURER, "1500");
        Employee e3 = new Employee("George", "Ionescu", "1234567890876", DidacticFunction.LECTURER, "3500");
        Employee e4 = new Employee("Bogdan", "Pacuraru", "1234567890876", DidacticFunction.ASISTENT, "4500");
        Employee e5 = new Employee("Vlad", "Georgescu", "1234567890876", DidacticFunction.TEACHER, "1500");
        Employee e6 = new Employee("Katya", "Puscas", "1234567890876", DidacticFunction.TEACHER, "23500");

        employeeList.add(e1);
        employeeList.add(e2);
        employeeList.add(e3);
        employeeList.add(e4);
        employeeList.add(e5);
        employeeList.add(e6);
    }

    @Override
    public boolean addEmployee(Employee employee) {
        if (employeeValidator.isValid(employee)) {
            employeeList.add(employee);
            return true;
        }
        return false;
    }

    @Override
    public void deleteEmployees() {
    }

    @Override
    public void modifyEmployee(Employee oldEmployee, Employee newEmployee) {
    }

    @Override
    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    @Override
    public Employee getEmployeeByCriteria(String criteria) {
        return null;
    }
}
