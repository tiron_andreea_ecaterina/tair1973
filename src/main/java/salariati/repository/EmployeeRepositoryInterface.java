package salariati.repository;

import java.util.List;

import salariati.model.Employee;

public interface EmployeeRepositoryInterface {

    boolean addEmployee(Employee employee);

    void deleteEmployees();

    void modifyEmployee(Employee oldEmployee, Employee newEmployee);

    List<Employee> getEmployeeList();

    Employee getEmployeeByCriteria(String criteria);
}
