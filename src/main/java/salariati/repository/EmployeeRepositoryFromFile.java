package salariati.repository;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import salariati.exception.EmployeeException;

import salariati.model.Employee;

import salariati.validator.EmployeeValidator;

public class EmployeeRepositoryFromFile implements EmployeeRepositoryInterface {

    private final String employeeDBFile = "employeeDB/employees.txt";
    private EmployeeValidator employeeValidator = new EmployeeValidator();

    @Override
    public boolean addEmployee(Employee employee) {
        if (employeeValidator.isValid(employee)) {
            BufferedWriter bw;
            try {
                bw = new BufferedWriter(new FileWriter(employeeDBFile, true));
                bw.write("");
                bw.write(employee.toString());
                bw.newLine();
                bw.close();
                return true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public void modifyEmployee(Employee oldEmployee, Employee newEmployee) {
        List<Employee> employeeList = getEmployeeList();
        int index = employeeList.indexOf(oldEmployee);
        if (index == -1)
            System.out.println("Nu s-a gasit salariatul");
        else {
            employeeList.remove(index);
            employeeList.add(newEmployee);
            deleteEmployees();
            for (Employee employee : employeeList)
                addEmployee(employee);
        }
    }

    @Override
    public List<Employee> getEmployeeList() {
        List<Employee> employeeList = new ArrayList<Employee>();

        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(employeeDBFile));
            String line;
            int counter = 0;
            while ((line = br.readLine()) != null) {
                Employee employee;
                try {
                    employee = Employee.getEmployeeFromString(line, counter);
                    employeeList.add(employee);
                } catch (EmployeeException ex) {
                    System.err.println("Error while reading: " + ex.toString());
                }
            }
        } catch (IOException e) {
           // System.err.println("Error while reading: " + e);
        } finally {
            if (br != null)
                try {
                    br.close();
                } catch (IOException e) {
                    System.err.println("Error while closing the file: " + e);
                }
        }

        return employeeList;
    }

    @Override
    public Employee getEmployeeByCriteria(String cnp) {
        List<Employee> employeeList = getEmployeeList();
        for (Employee employee : employeeList)
            if (employee.getCnp().equals(cnp))
                return employee;
        return null;
    }

    @Override
    public void deleteEmployees() {
        File file = new File(employeeDBFile);
        if (file.exists())
            file.delete();
    }
}
