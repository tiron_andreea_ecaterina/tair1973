package salariati.test.WBT;

import org.junit.Before;
import org.junit.Test;
import salariati.model.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.EmployeeRepositoryFromFile;
import salariati.repository.EmployeeRepositoryInterface;
import salariati.validator.EmployeeValidator;

import static org.junit.Assert.assertTrue;

public class TC03 {
    private EmployeeRepositoryInterface employeeRepository;
    private EmployeeValidator employeeValidator;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeRepositoryFromFile();
        employeeValidator = new EmployeeValidator();
    }

    @Test
    public void TCModifyEmployee() {
        Employee olderEmployee = new Employee("Popa", "Oana", "2991027330123", DidacticFunction.CONFERENT, "3000");
        Employee oldEmployee = new Employee("Popescu", "Ana", "2981027330345", DidacticFunction.ASISTENT, "2000");
        Employee newEmployee = new Employee("Popescu", "Ana", "2981027330345", DidacticFunction.TEACHER, "2000");

        assertTrue(employeeValidator.isValid(oldEmployee));
        assertTrue(employeeValidator.isValid(newEmployee));
        employeeRepository.addEmployee(olderEmployee);
        employeeRepository.addEmployee(oldEmployee);
        employeeRepository.modifyEmployee(oldEmployee, newEmployee);
        assertTrue(employeeRepository.getEmployeeByCriteria("2981027330345").getFunction().equals(DidacticFunction.TEACHER));
        employeeRepository.deleteEmployees();
    }
}