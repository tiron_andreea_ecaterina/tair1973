package salariati.test.WBT;

import org.junit.Before;
import org.junit.Test;
import salariati.model.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.EmployeeRepositoryFromFile;
import salariati.repository.EmployeeRepositoryInterface;
import salariati.validator.EmployeeValidator;

import static org.junit.Assert.assertTrue;

public class TC02 {
    private EmployeeRepositoryInterface employeeRepository;
    private EmployeeValidator employeeValidator;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeRepositoryFromFile();
        employeeValidator = new EmployeeValidator();
    }

    @Test
    public void TCModifyEmployee() {
        Employee oldEmployee = new Employee("Popescu", "Ana", "2981027330345", DidacticFunction.ASISTENT, "2000");
        Employee newEmployee = new Employee("Popescu", "Ana", "1981027330345", DidacticFunction.TEACHER, "2000");

        assertTrue(employeeValidator.isValid(oldEmployee));
        assertTrue(employeeValidator.isValid(newEmployee));
        employeeRepository.addEmployee(newEmployee);
        employeeRepository.modifyEmployee(oldEmployee,newEmployee);
        assertTrue(employeeRepository.getEmployeeByCriteria("1981027330345").equals(newEmployee));
        assertTrue(employeeRepository.getEmployeeList().size() == 1);
        employeeRepository.deleteEmployees();
    }
}
