package salariati.test.WBT;

import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.model.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.EmployeeRepositoryFromFile;
import salariati.repository.EmployeeRepositoryInterface;
import salariati.validator.EmployeeValidator;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class TC_valid {
    private EmployeeController employeeController;
    private EmployeeRepositoryInterface employeeRepository;
    private EmployeeValidator employeeValidator;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeRepositoryFromFile();
        employeeController = new EmployeeController(employeeRepository);
        employeeValidator = new EmployeeValidator();
    }

    @Test
    public void TCSortEmployees() {
        Employee employee1 = new Employee("Popa", "Oana", "2991027330123", DidacticFunction.CONFERENT, "3000");
        Employee employee2 = new Employee("Corman", "Gabriel", "2991227330345", DidacticFunction.ASISTENT, "2000");
        Employee employee3 = new Employee("Alexievici", "Vlad", "2981027330345", DidacticFunction.TEACHER, "2000");

        assertTrue(employeeValidator.isValid(employee1));
        assertTrue(employeeValidator.isValid(employee2));
        assertTrue(employeeValidator.isValid(employee3));
        employeeRepository.addEmployee(employee1);
        employeeRepository.addEmployee(employee2);
        employeeRepository.addEmployee(employee3);

        List<Employee> employeeList = employeeController.printAllSortedEmployees();
        assertTrue(employeeList.get(0).equals(employee1));
        assertTrue(employeeList.get(1).equals(employee2));
        assertTrue(employeeList.get(2).equals(employee3));
        employeeRepository.deleteEmployees();
    }
}
