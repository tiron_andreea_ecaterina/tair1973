package salariati.test.INT;

import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.model.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.EmployeeRepositoryFromFile;
import salariati.repository.EmployeeRepositoryInterface;
import salariati.validator.EmployeeValidator;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class TestBigBang {
    private EmployeeController employeeController;
    private EmployeeRepositoryInterface employeeRepository;
    private EmployeeValidator employeeValidator;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeRepositoryFromFile();
        employeeController = new EmployeeController(employeeRepository);
        employeeValidator = new EmployeeValidator();
    }

    @Test
    public void TestModulA() {
        Employee newEmployee = new Employee("Popescu","Ana", "2981027330345", DidacticFunction.ASISTENT, "2000");
        assertTrue(employeeValidator.isValid(newEmployee));
        assertTrue(employeeController.addEmployee(newEmployee));
        employeeRepository.deleteEmployees();
    }

    @Test
    public void TestModulB() {
        Employee oldEmployee = new Employee("Popa","Ana", "2981027330345", DidacticFunction.ASISTENT, "2000");
        Employee newEmployee = new Employee("Popa","Ana", "2981027330345", DidacticFunction.TEACHER, "2000");

        assertTrue(employeeValidator.isValid(oldEmployee));
        assertTrue(employeeValidator.isValid(newEmployee));
        employeeController.addEmployee(oldEmployee);
        employeeController.modifyEmployee("2981027330345",DidacticFunction.TEACHER);
        assertTrue(employeeRepository.getEmployeeByCriteria("2981027330345").getFunction().equals(DidacticFunction.TEACHER));
        employeeRepository.deleteEmployees();
    }

    @Test
    public void TestModulC() {
        Employee employee1 = new Employee("Alecu", "Oana", "2991027330123", DidacticFunction.CONFERENT, "3000");
        Employee employee2 = new Employee("Corman", "Gabriel", "2991227330345", DidacticFunction.ASISTENT, "2000");
        Employee employee3 = new Employee("Alexievici", "Vlad", "2981027330345", DidacticFunction.TEACHER, "2000");

        assertTrue(employeeValidator.isValid(employee1));
        assertTrue(employeeValidator.isValid(employee2));
        assertTrue(employeeValidator.isValid(employee3));
        employeeRepository.addEmployee(employee1);
        employeeRepository.addEmployee(employee2);
        employeeRepository.addEmployee(employee3);

        List<Employee> employeeList = employeeController.printAllSortedEmployees();
        assertTrue(employeeList.get(0).equals(employee1));
        assertTrue(employeeList.get(1).equals(employee2));
        assertTrue(employeeList.get(2).equals(employee3));
        employeeRepository.deleteEmployees();
    }

    @Test
    public void TestIntegrareABC() {
        Employee employee1 = new Employee("Alecu", "Oana", "2991027330123", DidacticFunction.CONFERENT, "3000");
        Employee employee2 = new Employee("Corman", "Gabriel", "2991227330345", DidacticFunction.ASISTENT, "2000");
        Employee employee3 = new Employee("Alexievici", "Vlad", "2981027330345", DidacticFunction.ASISTENT, "2000");

        //Testing Module A
        assertTrue(employeeRepository.addEmployee(employee1));
        assertTrue(employeeRepository.addEmployee(employee2));
        assertTrue(employeeRepository.addEmployee(employee3));

        //Testing Module B
        employeeController.modifyEmployee("2981027330345",DidacticFunction.TEACHER);
        assertTrue(employeeRepository.getEmployeeByCriteria("2981027330345").getFunction().equals(DidacticFunction.TEACHER));
        employeeController.modifyEmployee("2981027330345",DidacticFunction.ASISTENT);

        //Testing Module C
        List<Employee> employeeList = employeeController.printAllSortedEmployees();
        assertTrue(employeeList.get(0).equals(employee1));
        assertTrue(employeeList.get(1).equals(employee2));
        assertTrue(employeeList.get(2).equals(employee3));
        employeeRepository.deleteEmployees();
    }
}
