package salariati.test.BBT;

import org.junit.Before;
import org.junit.Test;
import salariati.model.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.EmployeeRepositoryInterface;
import salariati.repository.EmployeeRepositoryMock;
import salariati.validator.EmployeeValidator;

import static org.junit.Assert.assertTrue;

public class TC21_BVA{
    private EmployeeRepositoryInterface employeeRepository;
    private EmployeeValidator employeeValidator;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeRepositoryMock();
        employeeValidator  = new EmployeeValidator();
    }

    @Test
    public void TCAddNewEmployee() {
        Employee newEmployee = new Employee("Popescu","Ana", "2981027330345", DidacticFunction.TEACHER, "123");
        assertTrue(employeeValidator.isValid(newEmployee));
        assertTrue(employeeRepository.addEmployee(newEmployee));
    }
}
